# OpenML dataset: SigmaCabPrediction

https://www.openml.org/d/43609

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Problem Statement
Welcome to Sigma Cab Private Limited - a cab aggregator service. Their customers can download their app on smartphones and book a cab from any where in the cities they operate in. They, in turn search for cabs from various service providers and provide the best option to their client across available options. They have been in operation for little less than a year now. During this period, they have captured surgepricingtype from the service providers. 
You have been hired by Sigma Cabs as a Data Scientist and have been asked to build a predictive model, which could help them in predicting the surgepricingtype pro-actively. This would in turn help them in matching the right cabs with the right customers quickly and efficiently. 
Data
Variable Definition
TripID - ID for TRIP (Can not be used for purposes of modelling)
TripDistance - The distance for the trip requested by the customer
TypeofCab - Category of the cab requested by the customer
CustomerSinceMonths - Customer using cab services since n months; 0 month means current month
LifeStyleIndex - Proprietary index created by Sigma Cabs showing lifestyle of the customer based on their behaviour
ConfidenceLifeStyleIndex - Category showing confidence on the index mentioned above
DestinationType - Sigma Cabs divides any destination in one of the 14 categories
CustomerRating - Average of life time ratings of the customer till date
CancellationLast1Month - Number of trips cancelled by the customer in last 1 month
Var1, Var2 and Var3 - Continuous variables masked by the company. Can be used for modelling purposes
Gender - Gender of the customer
SurgePricing_Type - Predictor variable can be of 3 types

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43609) of an [OpenML dataset](https://www.openml.org/d/43609). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43609/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43609/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43609/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

